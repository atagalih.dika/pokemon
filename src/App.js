import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';

import { MyPokemonProvider } from './contexts/myPokemon';

import Layout from './components/Layout';
import PokemonList from './routes/PokemonList';
import PokemonDetail from './routes/PokemonDetail';
import MyPokemonList from './routes/MyPokemonList';

import useGqlClient from './hooks/useGqlClient';

const GQL_URL = 'https://graphql-pokeapi.vercel.app/api/graphql';

function App() {
  const client = useGqlClient(GQL_URL);

  return (
    <ApolloProvider client={client}>
      <Router>
        <Layout>
          <MyPokemonProvider>
            <Switch>
              <Route exact path="/" >
                <PokemonList/>
              </Route>
              <Route path="/detail/:name">
                <PokemonDetail/>
              </Route>
              <Route>
                <MyPokemonList path="/my-pokemon"/>
              </Route>
            </Switch>
          </MyPokemonProvider>
        </Layout>
      </Router>
    </ApolloProvider>
  );
}

export default App;
