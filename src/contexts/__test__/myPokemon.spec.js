import { render } from '@testing-library/react';
import React, {useEffect} from 'react';

import { MyPokemonProvider, useMyPokemon } from '../myPokemon';

const DUMMY_POKEMON = {name: 'poke1', imageUrl: ''};
const DUMMY_POKEMON_NICKNAME = 'pokeNick';

const ChildComponent = () => {
  const { catchPokemon, releasePokemon } = useMyPokemon();
  useEffect(() => {
    catchPokemon(DUMMY_POKEMON, DUMMY_POKEMON_NICKNAME);
    releasePokemon(DUMMY_POKEMON.name, DUMMY_POKEMON_NICKNAME);
  }, [catchPokemon, releasePokemon])
  return null 
}

jest.spyOn(window.localStorage.__proto__, 'setItem');
jest.spyOn(window.localStorage.__proto__, 'removeItem');

describe('myPokemon context test', () => {
  test('should able to persist data by local storage when catch and release pokemon', () => {
    
    render(<MyPokemonProvider>
      <ChildComponent/>
    </MyPokemonProvider>)

    expect(localStorage.setItem).toHaveBeenCalledTimes(2);
  });
})