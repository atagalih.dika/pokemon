import React, { createContext, useContext, useReducer, useEffect } from 'react';

const MyPokemonContext = createContext({});

const STORAGE_KEY = 'POKEMON_STATE';

const initialState = {
  myPokemons: [],
  pokemonOwned: {}
}

const ACTION_TYPE = {
  CATCH_POKEMON: 'CATCH_POKEMON',
  RELEASE_POKEMON: 'RELEASE_POKEMON',
  RELOAD_POKEMON: 'RELOAD_POKEMON'
}

const myPokemonReducer = (state, action) => {
  switch(action.type) {
    case ACTION_TYPE.CATCH_POKEMON:{
      const newState = { 
        myPokemons: [...state.myPokemons, action.pokemon],
        pokemonOwned: { 
          ...state.pokemonOwned, 
          [action.pokemon.name]: (state.pokemonOwned[action.pokemon.name] || 0) + 1
        }
      }

      localStorage.setItem(STORAGE_KEY, JSON.stringify(newState));
      return newState;
    }
    case ACTION_TYPE.RELEASE_POKEMON:{
      const newState = {
        myPokemons: state.myPokemons.filter(({nickName}) => nickName !== action.nickName),
        pokemonOwned: { 
          ...state.pokemonOwned, 
          [action.name]: Math.max((state.pokemonOwned[action.name] || 0) - 1, 0)
        }
      };

      localStorage.setItem(STORAGE_KEY, JSON.stringify(newState));
      return newState;
    }
    case ACTION_TYPE.RELOAD_POKEMON:
      return JSON.parse(localStorage.getItem(STORAGE_KEY)) || state;
    default:
      return state
  }
}

const MyPokemonProvider = ({ children }) => {
  const [ state, dispatch ] = useReducer(myPokemonReducer, initialState);

  useEffect(() => {
    dispatch({ type: ACTION_TYPE.RELOAD_POKEMON });
  }, [])

  const catchPokemon = (pokemon, nickName) => {
    dispatch({ type: ACTION_TYPE.CATCH_POKEMON, pokemon: {...pokemon, nickName} });
  }

  const releasePokemon = (name, nickName) => {
    dispatch({ type: ACTION_TYPE.RELEASE_POKEMON, name, nickName });
  }

  const isNickNameAlreadyExist = (nickName) => {
    return state.myPokemons.some((pokemon) => pokemon.nickName === nickName);
  }

  const { myPokemons, pokemonOwned } = state;

  return (
    <MyPokemonContext.Provider value={{
      myPokemons,
      pokemonOwned,
      catchPokemon,
      releasePokemon,
      isNickNameAlreadyExist
    }}>
      {children}
    </MyPokemonContext.Provider>
  )
}

const useMyPokemon = () => {
  return useContext(MyPokemonContext);
}

export { MyPokemonProvider, useMyPokemon }