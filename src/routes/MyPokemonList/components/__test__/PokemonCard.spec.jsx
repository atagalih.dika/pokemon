import React from 'react';
import { cleanup, fireEvent, render } from '@testing-library/react';
import PokemonCard from '../PokemonCard';

describe('MyPokemonList PokemonCard test', () => {
  afterEach(cleanup);

  test('should show given name', () => {
    const { getByText } = render(<PokemonCard name={'card 1'}/>);
    getByText('card 1');
  })

  test('should able to invoke onRemove when click remove icon', () => {
    const mockOnRemove = jest.fn();
    const { getByTestId } = render(<PokemonCard onRemove={mockOnRemove}/>);
    
    const removeIcon = getByTestId('remove-icon');
    expect(removeIcon).toBeTruthy();

    fireEvent.click(removeIcon);

    expect(mockOnRemove).toBeCalled();
  }) 
})