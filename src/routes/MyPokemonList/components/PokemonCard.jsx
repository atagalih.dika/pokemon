/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { forwardRef } from 'react';
import PropTypes from 'prop-types';

import mq from '../../../utilities/mediaQueries';

import { MdCancel } from 'react-icons/md';
import Card from "../../../components/Card";

const PokemonCard = forwardRef(({ name, imageUrl, onRemove }, ref) => {
  return (
    <Card 
      ref={ref} 
      color='#3DB2FF' 
      cssExtend = {{
        overflow: 'visible'
      }}
    >
      <div
        css={{
          display: 'flex',
          flexDirection: 'column',
          position: 'relative',
          height: '100%',
        }}
      >
        <h4
          css={mq({
            fontSize: '1em',
            textShadow: '1px 1px #3DB2FF',
            textTransform: 'capitalize',
            margin: '0px 0px 4px 0px',
            color: 'white'
          })}
        >
          {name}
        </h4>
        <img 
          css={mq({
            width: '80px',
            alignSelf: 'flex-end',
            borderRadius: '40% 20% 40% 20%',
            backgroundColor: 'rgba(20, 20, 200, 0.3)'
          })}
          src={imageUrl} 
          alt='pokemon' 
        />
        <MdCancel
          css={mq({
            position: 'absolute',
            top: ['-22px'],
            right: ['-22px'],
            padding: ['4px'],
            borderRadius: '50%',
            textAlign: 'center',
            fontSize: ['2em'],
            color: 'red'
          })}
          data-testid='remove-icon'
          onClick={onRemove}
        />
      </div>
    </Card>
  )
})

PokemonCard.propTypes = {
  name: PropTypes.string,
  imageUrl: PropTypes.string,
  count: PropTypes.number
}

PokemonCard.defaultProps = {
  count: 0
}

export default PokemonCard;