import { useMyPokemon } from '../../contexts/myPokemon';

import PokemonCard from "./components/PokemonCard";

const MyPokemonList = () => {
  const { myPokemons, releasePokemon } = useMyPokemon();
  return(
    <div>
      {
        myPokemons.map(({ name, nickName, imageUrl }) => (
          <PokemonCard 
            key={nickName} 
            name={nickName} 
            imageUrl={imageUrl} 
            onRemove={() => releasePokemon(name, nickName)} 
          />
        ))
      }
    </div>
  )
}

export default MyPokemonList;