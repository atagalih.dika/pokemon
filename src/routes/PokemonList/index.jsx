import { useRef, useCallback } from 'react';
import { Link } from 'react-router-dom';

import { useMyPokemon } from '../../contexts/myPokemon';

import PokemonCard from './components/PokemonCard';

import usePokemons from './hooks/usePokemons';

const PokemonList = () => {
  const { pokemons, fetchMore, loading, hasMore } = usePokemons();

  const { pokemonOwned } = useMyPokemon();

  const observer = useRef();
  const lastElementRef = useCallback((node) => {
    if(loading) return;
    if(observer.current) observer.current.disconnect();
    observer.current = new IntersectionObserver((entries) => {
      if(entries[0].isIntersecting && hasMore) {
        fetchMore();
      }
    });
    if(node) observer.current.observe(node);
  }, [loading, hasMore, fetchMore])
  
  return(
    <div>
      {
        pokemons.map(({id, name, artwork}, index) => (
          <Link key={id} to={`/detail/${name}`}>
            <PokemonCard
              key={id}
              ref={index === pokemons.length - 1 ? lastElementRef : null} 
              name={name} 
              imageUrl={artwork}
              count={pokemonOwned[name] || 0}
            />
          </Link>
        ))
      }
    </div>
  )
}

export default PokemonList;