import { useState, useCallback } from 'react';
import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';

const POKEMONS_QUERY = loader('../../../queries/pokemons.graphql');

const LIST_LIMIT = 10;

const usePokemons = () => {
  const [ pokemons, setPokemons ] = useState([]);
  const [ offset, setOffset ] = useState(0);
  const [ hasMore, setHasMore ] = useState(false);

  const { data, loading } = useQuery(POKEMONS_QUERY, {
    variables: {
      limit: LIST_LIMIT,
      offset
    },
    onCompleted: () => {
      const pokemons = data?.pokemons?.results || [];
      const hasMore = !!data?.pokemons?.next;

      setPokemons((prev) => [...prev, ...pokemons]);
      setHasMore(hasMore);
    }
  });

  const fetchMore = useCallback(() => {
    setOffset((prev) => prev + LIST_LIMIT);
  }, [])

  return { pokemons, fetchMore, loading, hasMore };
}

export default usePokemons;