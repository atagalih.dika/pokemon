/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { forwardRef } from 'react';
import PropTypes from 'prop-types';

import mq from '../../../utilities/mediaQueries';

import Card from "../../../components/Card";

const PokemonCard = forwardRef(({ name, imageUrl, count }, ref) => {
  return (
    <Card ref={ref} color='#40ff00'>
      <div
        css={{
          display: 'flex',
          flexDirection: 'column',
          position: 'relative',
          height: '100%',
        }}
      >
        <h4
          css={mq({
            fontSize: '1em',
            textShadow: '1px 1px green',
            textTransform: 'capitalize',
            margin: '0px 0px 4px 0px',
            color: 'white'
          })}
        >
          {name}
        </h4>
        <img 
          css={mq({
            width: '80px',
            alignSelf: 'flex-end',
            borderRadius: '40% 20% 40% 20%',
            backgroundColor: 'rgba(20, 200, 20, 0.3)'
          })}
          src={imageUrl} 
          alt='pokemon' 
        />
        <div
          css={mq({
            position: 'absolute',
            bottom: ['10px'],
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          })}
        >
          <span
            css={mq({
              backgroundColor: '#AE00FB',
              color: 'white',
              padding: ['4px'],
              width: ['20px'],
              borderRadius: '50%',
              textAlign: 'center'
            })}
          >
            {count}
          </span>
          <span
            css={{
              fontSize: '0.7em',
              color: 'gray'
            }}
          >
            owned
          </span>
        </div>
      </div>
    </Card>
  )
})

PokemonCard.propTypes = {
  name: PropTypes.string,
  imageUrl: PropTypes.string,
  count: PropTypes.number
}

PokemonCard.defaultProps = {
  count: 0
}

export default PokemonCard;