/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import Carousel from '../../../components/Carousel';
import Card from '../../../components/Card';

import pokeballImg from '../../../assets/pokeball.jpeg';

const cssImage = {
  width: '100%',
  margin: 'auto',
  display: 'block',
  position: 'relative'
}

const PictureCarousel = ({ pictures }) => {
  return (
    <Carousel
      carouselItems={pictures}
      renderItem={(item) => (
        <Card
          cssBackground={{
            backgroundImage: `url(${pokeballImg})`,
            backgroundSize: '200px',
            opacity: 0.5
          }}>
          <img 
            css={cssImage}
            src={item} 
            alt='pokemon'
          />
        </Card>
      )}
    >

    </Carousel>
  )
}

export default PictureCarousel;