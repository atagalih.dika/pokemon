/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { MdCancel } from 'react-icons/md';
import Card from '../../../components/Card';

import mq from '../../../utilities/mediaQueries';

const ModalInput = ({ show, onClose, onSubmit, validation }) => {
  return(
    <Card 
      cssExtend={{
        position: ['fixed', 'fixed', 'fixed', 'absolute'],
        top: ['', '', '', 0],
        bottom: show ? ['-20px', '-20px', '-20px', 0] : ['-30px', '-30px', '-30px', '10px'],
        left: 0,
        right: ['', '', '', 0],
        margin: ['', '', '', 'auto'],
        paddingLeft: ['20px', '20px', '20px', 0],
        background: 'white',
        width: ['100%', '100%', '100%', '300px'],
        height: ['200px'],
        transition: 'opacity 0.4s, bottom 0.4s',
        display: show ? 'block' : 'none',
        textAlign: 'center'
      }}
    >
      <form
        css={mq({
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          alignItems: ['flex-start', 'flex-start', 'flex-start', 'center']
        })}
        onSubmit={(e) => {
          e.preventDefault();

          const value = e.target['nick-name'].value;
          if(!validation(value)){
            onSubmit(value);
            onClose()
          }
        }}
      >
        <MdCancel 
          css={{
            position: 'absolute',
            top: 0,
            right: 0
          }}
          onClick={onClose}
        />
        <h4>You got the pokemon, pick a nick name !</h4>
        <input name='nick-name' defaultValue=''/>
        <button 
          css={mq({
            margin: ['20px 0']
          })}
          type='submit'
        >
          Submit
        </button>
      </form>
    </Card>
  )
}

export default ModalInput;