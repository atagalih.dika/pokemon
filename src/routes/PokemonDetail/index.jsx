/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { useState } from 'react';
import { useParams } from 'react-router-dom';

import List from '../../components/List';
import PictureCarousel from './components/PictureCarousel';
import ModalInput from './components/ModalInput';

import bg from '../../assets/bg.jpg';
import pokeball from '../../assets/pokeball.jpeg';

import usePokemon from './hooks/usePokemon';
import { useMyPokemon } from '../../contexts/myPokemon';
import mq from '../../utilities/mediaQueries';

const cssList = {
  height: ['100%', '100%', '100%', '550px']
}

const PokemonDetail = () => {
  const [showModal, setShowModal] = useState(false);

  const { name } = useParams();
  const { pictures, moves, types, loading } = usePokemon(name);
  const { isNickNameAlreadyExist, catchPokemon } = useMyPokemon();

  if(loading){
    return <div>Loading...</div>;
  }

  const doCatch = () => {
    const randomNum = Math.floor(Math.random() * 10) % 2;
    const isEven = randomNum % 2 === 0;

    setShowModal(isEven);
  }

  return(
    <div>
      <PictureCarousel pictures={pictures}/>
      <div
        css={mq({
          display: ['block', 'block', 'block', 'flex']
        })}
      >
        <List 
          title='Types'
          cssExtend={cssList}
          cssBackground={{
            backgroundImage: `url(${bg})`,
            opacity: 0.3
          }}
        >
          {
            types.map(({ type }) => (
              <List.Item key={type.name}>{type.name}</List.Item>
            ))
          }
        </List>
        <List 
          title='Moves'
          cssExtend={cssList}
          cssBackground={{
            backgroundImage: `url(${bg})`,
            opacity: 0.3
          }}
        >
          {
            moves.map(({ move }) => (
              <List.Item key={move.name}>{move.name.replace('-', ' ')}</List.Item>
            ))
          }
        </List>
      </div>
      <button
        css={mq({
          position: 'fixed',
          bottom: '15px',
          right: ['10px', '20px', '30px', '50px', '100px'],
          width: ['60px', '70px', '80px', '90px', '100px'],
          height: ['60px', '70px', '80px', '90px', '100px'],
          borderRadius: '100%',
          background: `url(${pokeball})`,
          backgroundSize: ['110px', '125px', '140px', '160px', '175px'],
          backgroundPosition: 'center',
          border: 'none',
          outline: 'none',
          color: '#FFF',
          fontSize: ['0.7em'],
          fontWeight: 600,
          textShadow: '1px 1px red',
          boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)',
          transition: '0.3s',
          cursor: 'pointer'
        })}
        onClick={doCatch}
      >
        CATCH !
      </button>
      <ModalInput 
        show={showModal}
        onClose={() => setShowModal(false)}
        validation={isNickNameAlreadyExist}
        onSubmit={(value) => catchPokemon({ name, imageUrl: pictures[0] }, value)} 
      />
    </div>
  )
}

export default PokemonDetail;