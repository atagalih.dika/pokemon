import { useQuery } from '@apollo/client';
import { loader } from 'graphql.macro';

const POKEMON_QUERY = loader('../../../queries/pokemon.graphql');

const usePokemon = (name) => {
  const { data, loading } = useQuery(POKEMON_QUERY, {
    variables: {
      name
    }
  });

  const picturesObj = data?.pokemon?.sprites || {};
  const moves = data?.pokemon?.moves || [];
  const types = data?.pokemon?.types || [];

  const picturesArr = Object.values(picturesObj);
  picturesArr.shift();
  const pictures = picturesArr.filter((picture) => picture);

  return { pictures, moves, types, loading };
}

export default usePokemon;