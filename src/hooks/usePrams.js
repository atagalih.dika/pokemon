import { matchPath } from 'react-router';
import { useLocation } from 'react-router-dom';

const useParams = (path) => {
  const { pathname } = useLocation();
  const match = matchPath(pathname, { path });
  return match?.params || {};
}

export default useParams;