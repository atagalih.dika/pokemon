import { useMemo } from 'react';
import { ApolloClient, InMemoryCache, HttpLink, from } from '@apollo/client';
import { onError } from '@apollo/client/link/error';

const errorLink = onError(({ graphqlErrors, networkError }) => {
  if(graphqlErrors) {
    // eslint-disable-next-line array-callback-return
    graphqlErrors.map(({ message, location, path }) => {
      alert(`Graphql error ${message}`)
    })
  }
});

const link = (uri) => from([
  errorLink,
  new HttpLink({ uri })
]);

const useGqlClient = (uri) => {

  const client = useMemo(() => (
    new ApolloClient({
      cache: new InMemoryCache(),
      link: link(uri)
    })
  ), [uri]);

  return client;
}

export default useGqlClient;