import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import useParams from './usePrams';

const TITLE_MAP = {
  '/': 'Pokemon List',
  '/my-pokemon': 'My Pokemon List'
}

const NAV_MAP = {
  '/': [{ name: 'My Pokemon', path: '/my-pokemon'}],
  '/my-pokemon': [{name: 'Home', path: '/'}]
}

const useTitle = () => {
  const location = useLocation();
  const { name } = useParams('/detail/:name');

  const title = useMemo(() => TITLE_MAP[location.pathname], [location]) || name;
  const nav = useMemo(() => NAV_MAP[location.pathname], [location]) || [{name: 'Home', path: '/'}, { name: 'My Pokemon', path: '/my-pokemon'}];

  return { title: title, nav };
}

export default useTitle;