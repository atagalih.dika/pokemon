import React from 'react';
import { render } from '@testing-library/react';
import useParams from '../usePrams';

const TestCompoenent = () => {
  const { name } = useParams()
  return (
    <div>
      {name}
    </div>
  );
}

jest.mock('react-router-dom', () => {
  const useLocation = () => {
    return '/detail/ivysaur'
  }

  return { useLocation };
})

jest.mock('react-router', () => {
  const matchPath = () => {
    return  { params: { name: 'ivysaur'}};
  }

  return { matchPath };
})

describe('useParams test', () => {
  test('should return desired value', () => {
    const { getByText } = render(<TestCompoenent />);
    getByText('ivysaur')
  })
})