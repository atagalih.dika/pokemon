import React from 'react';
import { render } from '@testing-library/react';

import useTitle from '../useTitle';

const TestComponent = () => {
  const { title } = useTitle();

  return (
    <div>{title}</div>
  )
}

jest.mock('react-router-dom', () => {
  const useLocation = () => {
    return { pathname: '/' }
  }

  return { useLocation };
})

jest.mock('../usePrams', () => {
  return () => { return {} };
})

test('the title should be Pokemon List on home page', () => {
  const { getByText } = render(<TestComponent/>)
  getByText('Pokemon List');
})

jest.mock('react-router-dom', () => {
  const useLocation = () => {
    return { pathname: '/my-pokemon' }
  }

  return { useLocation };
})

jest.mock('../usePrams', () => {
  return () => { return {} };
})

test('the title should be My Pokemon List on my-pokemon page', () => {
  const { getByText } = render(<TestComponent/>)
  getByText('My Pokemon List');
})