import facepaint from 'facepaint';

const breakPoints = [576, 768, 992, 1200];

const mq = facepaint(
  breakPoints.map(bp => `@media (min-width: ${bp}px)`)
);

export default mq;

