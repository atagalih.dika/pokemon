/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import mq from '../utilities/mediaQueries';
import useTitle from '../hooks/useTitle';

const Layout = ({ children, showTitle }) => {
  const { title, nav } = useTitle();

  return (
    <div
      css={mq({
        padding: ['0px 20px', '0px 30px', '0px 40px', '0px 100px', '0px 150px'],
        backgroundColor: '#F4F9F9',
        height: '100%',
        minHeight: '100vh',
        fontSize: ['16px', '17px', '18px', '19px', '20px']
      })}
    >
      <div
        css={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        {
          showTitle && 
          <h1
            css={mq({
              fontSize: ['1.5em', '1.6em', '1.8em', '2em'],
              marginTop: 0,
              paddingTop: '10px',
              textTransform: 'capitalize'
            })}
          >
            {title}
          </h1>
        }
        <div>
          {
            nav.map(({name, path}) => (
              <Link 
                css={mq({
                  display: 'inline-block',
                  margin: ['10px 3px','10px 5px', '10px 7px', '10px'],
                  fontSize: ['0.8em', '1em']
                })}
                key={name} 
                to={path}
              >
                {name}
              </Link>
            ))
          }
        </div>
      </div>
      {children}
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  showTitle: PropTypes.bool
}

Layout.defaultProps = {
  showTitle: true
}

export default Layout;