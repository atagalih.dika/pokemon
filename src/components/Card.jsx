/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { forwardRef } from 'react';
import PropTypes from 'prop-types';

import mq from '../utilities/mediaQueries';


const Card = forwardRef(({ children, color, cssExtend, cssBackground }, ref) => {
  return (
    <div
      ref={ref}
      css={mq({
        display: 'inline-block',
        width: ['130px'],
        height: ['110px'],
        margin: '4px',
        borderRadius: '15px',
        padding: '10px 7px 4px',
        boxSizing: 'border-box',
        overflow: 'hidden',
        boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
        transition: '0.3s',
        cursor: 'pointer',
        position: 'relative',
        ...cssExtend
      })}
    >
      <div
        css={{
          backgroundColor: color,
          position: 'absolute',
          top: '0px',
          bottom: '0px',
          left: '0px',
          right: '0px',
          borderRadius: '15px',
          pointerEvents: 'none',
          ...cssBackground
        }}
      />
      {children}
    </div>
  )
})

Card.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  color: PropTypes.string,
}

export default Card;