/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { useState, useEffect, Fragment } from 'react';
import { cloneDeep } from 'lodash';

import { MdChevronLeft, MdChevronRight } from 'react-icons/md'

import mq from '../utilities/mediaQueries';

const cssArrowIcon = {
  height: '70px',
  width: '70px',
  cursor: 'pointer',
  color: '#AE00FB',
  position: 'absolute'
}

const Carousel = ({ renderItem, cssIcon, height = 120, carouselItems }) => {
  const [items, setItems] = useState(carouselItems);

  useEffect(() => {
    setItems(carouselItems);
  }, [carouselItems])
  
  const onClickPrev = () => {
    const newItems = cloneDeep(items);
    const firstItem = newItems.shift();
    newItems.push(firstItem);

    setItems(newItems);
  }

  const onClickNext = () => {
    const newItems = cloneDeep(items);
    const lastItem = newItems.pop();
    newItems.unshift(lastItem);

    setItems(newItems);
  }

  return (
    <div
      css={{
        position: 'relative',
        height: `${height}px`,
        width: '100%',
        display: 'flex',
      }}
    >
      <MdChevronLeft
        css={mq({
          ...cssArrowIcon,
          ...cssIcon,
          top: `${height/4}px`,
          left: ['0px']
        })}
        onClick={onClickPrev}
      />
      <div
        css={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-around',
          overflow: 'hidden',
          padding: '0px 50px',
          width: '100%'
        }}
      >
       {
         items.map((item) => (
           <Fragment key={item}>
            {renderItem(item)}
           </Fragment>
         ))
       }
      </div>
      <MdChevronRight
        css={mq({
          ...cssArrowIcon,
          ...cssIcon,
          top: `${height/4}px`,
          right: '0px'
        })}
        onClick={onClickNext}
      />
    </div>
  )
}

export default Carousel;