/** @jsxRuntime classic /
/* @jsx jsx */
import { jsx } from '@emotion/react';

import { Fragment } from 'react';
import mq from '../utilities/mediaQueries';

import Card from './Card';

const ListItem = ({ children }) => {
  return (
    <div
      css={{
        fontSize: '0.9em',
        color: 'tomato',
        textTransform: 'capitalize'
      }}
    >
      {children}
    </div>
  )
}

const List = ({ title, children, cssExtend, cssBackground, cssListItem }) => {
  return(
    <Card
      cssExtend={{
        width: '100%',
        height: 'fit-content',
        marginTop: '20px',
        padding: '20px',
        ...cssExtend
      }}
      cssBackground={cssBackground}
    >
      {
        title && (
          <Fragment>
            <h4
              css={{
                margin: '0',
                color: 'red'
              }}
            >
              {title}
            </h4>
            <div 
              css={{
                height: '1px',
                width: '100%',
                margin: '10px 0',
                backgroundColor: 'violet',
              }}
            />
          </Fragment>
        )
      }
      <div
        css={mq({
          display: 'flex',
          flexDirection: 'column',
          flexWrap: 'wrap',
          height: '100%',
          ...cssListItem
        })}
      >
        {children}
      </div>
    </Card>
  )
}

List.Item = ListItem;

export default List;